import React from "react";
import cookie from 'react-cookies'
import "bootstrap/dist/css/bootstrap.css";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formValues: {
        email: "",
        password: ""
      },
      formErrors: {
        email: "",
        password: ""
      },
      formValidity: {
        email: false,
        password: false
      },
      isSubmitting: false,
      formdatafromcookie: null,
      formdatafromcookie: null,
      isDisconnected: false,
      OfflineData: {},
      backend: {},
      savedStats: Boolean,
      nonConnected: Boolean,
      isSuccess: Boolean,
      show: false
    };
  }

  componentDidMount() {
    this.handleConnectionChange();
    window.addEventListener('online', this.handleConnectionChange);
    window.addEventListener('offline', this.handleConnectionChange);
  }

  componentWillUnmount() {
    window.removeEventListener('online', this.handleConnectionChange);
    window.removeEventListener('offline', this.handleConnectionChange);
  }

  handleConnectionChange = () => {
    const { backend, savedStats, formdatafromcookie } = this.state;

    this.setState({ isDisconnected: navigator.onLine })
    this.setState({ isSuccess: false })

    if (!savedStats) {
      debugger
      this.setState({ formdatafromcookie: cookie.loadAll() })
      this.setState({ backend: formdatafromcookie })
      this.setState({ savedStats: true })
      this.setState({ isSuccess: true })
      this.onClearCookie()

    }
  }

  handleChange = ({ target }) => {
    const { formValues } = this.state;
    formValues[target.name] = target.value;
    this.setState({ formValues });
    this.handleValidation(target);
  };

  handleValidation = target => {
    const { name, value } = target;
    const fieldValidationErrors = this.state.formErrors;
    const validity = this.state.formValidity;
    const isEmail = name === "email";
    const isPassword = name === "password";
    const emailTest = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    validity[name] = value.length > 0;
    fieldValidationErrors[name] = validity[name]
      ? ""
      : `${name} is required and cannot be empty`;

    if (validity[name]) {
      if (isEmail) {
        validity[name] = emailTest.test(value);
        fieldValidationErrors[name] = validity[name]
          ? ""
          : `${name} should be a valid email address`;
      }
      if (isPassword) {
        validity[name] = value.length >= 3;
        fieldValidationErrors[name] = validity[name]
          ? ""
          : `${name} should be 3 characters minimum`;
      }
    }

    this.setState({
      formErrors: fieldValidationErrors,
      formValidity: validity
    });
  };


  handleSubmit = event => {
    event.preventDefault();
    const { isDisconnected, OfflineData } = this.state;
    this.setState({ isSubmitting: true });
    const { formValues, formValidity } = this.state;
    if (Object.values(formValidity).every(Boolean)) {
      alert("Form is validated! Submitting the form...");
      this.setState({ isSubmitting: false });
    } else {
      for (let key in formValues) {
        let target = {
          name: key,
          value: formValues[key]
        };
        this.handleValidation(target);
      }
      this.setState({ isSubmitting: false });
    }
    console.log('formValues', formValues)
    const expires = new Date()
    expires.setDate(Date.now() + 1000 * 30)
    this.setState({ OfflineData: formValues })
    console.log('OfflineData', OfflineData)
    debugger;
    if (!isDisconnected) {
      cookie.save(formValues.email, formValues.password)
      this.setState({ savedStats: false })
      this.setState({ notConnected: true })
      this.setState({ show: true })

    } else {
      this.setState({ backend: formValues })
      this.setState({ savedStats: true })
      this.setState({ show: true })
      this.onClearCookie()
    }
  };
  onClearCookie = () => {
    cookie.remove(this.state.formValues.email)
  }

  handleClose = () => {
    this.setState({ show: false })
  }
  render() {
    const { formValues, formErrors, isSubmitting } = this.state;
    return (
      <div className="container">
        <div className="row mb-5">
          <div className="col-lg-12 text-center">
            <h1 className="mt-5">Login Form</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label>Email address</label>
                <input
                  type="email"
                  name="email"
                  className={`form-control ${
                    formErrors.email ? "is-invalid" : ""
                    }`}
                  placeholder="Enter email"
                  onChange={this.handleChange}
                  value={formValues.email}
                />
                <div className="invalid-feedback">{formErrors.email}</div>
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  name="password"
                  className={`form-control ${
                    formErrors.password ? "is-invalid" : ""
                    }`}
                  placeholder="Password"
                  onChange={this.handleChange}
                  value={formValues.password}
                />
                <div className="invalid-feedback">{formErrors.password}</div>
              </div>
              <button
                type="submit"
                className="btn btn-primary btn-block"
                disabled={isSubmitting}
              >
                {isSubmitting ? "Please wait..." : "Submit"}
              </button>
            </form>

          </div>
        </div>
        {this.state.backend && this.state.backend.email &&

          <div style={{ background: 'green', marginTop: 30, color: 'white' }}> Login credentials data email : {this.state.backend && this.state.backend.email} and password  : {this.state.backend && this.state.backend.password} are successfully saved to database
        </div>

        }
        {this.state.notConnected &&
          <div style={{ background: 'Yellow', marginTop: 30, }}> Please check your Internet connection! your data is saved when we found Connectivity your data is saved to database </div>
        }
        {this.state.isSuccess &&
          <div style={{ background: 'green', marginTop: 30, color: 'white' }}> Login credentials successfully saved to database </div>
        }

      </div>
    );
  }
}
export default LoginForm
